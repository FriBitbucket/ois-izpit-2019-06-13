if (!process.env.PORT)
  process.env.PORT = 8080;


// Priprava strežnika
var express = require('express');
var streznik = express();
streznik.set('view engine', 'ejs');
streznik.use(express.static('public'));


var vremenske_slike = {
  "oblačno": "iconfinder_05_cloud_smile_cloudy_emoticon_weather_smiley_3375695.png",
  "deževno": "iconfinder_06_rain_cloud_cry_emoticon_weather_smiley_3375694.png",
  "sončno": "iconfinder_07_sun_smile_happy_emoticon_weather_smiley_3375693.png",
  "sneženo": "iconfinder_10_snow_coud_emoticon_weather_smiley_3375690.png",
  "pretežno oblačno": "iconfinder_09_cloudy_sun_happy_emoticon_weather_smiley_3375691.png"
};


var seznam_krajev = [];


// Prikaz zemljevida vremena
streznik.get("/", function (zahteva, odgovor) {
  odgovor.redirect('/zemljevid-vremena');
});


// Storitev, ki vrne vremenske podatke za podano poštno številko
streznik.get("/vreme/:postnaStevilka", function(zahteva, odgovor) {
  var postnaStevilka = parseInt(zahteva.params.postnaStevilka, 10);
  if (postnaStevilka == undefined || isNaN(postnaStevilka)) {
    odgovor.send(404, "Manjka ustrezna poštna številka!");
  } else {
    
    var mOpis = "";
    var mTemp = 0;
    
    var digit = firstDigit(postnaStevilka);
    
    switch(digit) {
      case 1:
      case 4:
        mOpis = "deževno";
        mTemp = getRandomArbitrary(5, 20);
        break;
      case 2:
      case 3:
      case 9:
        mOpis = "sončno";
        mTemp = getRandomArbitrary(20, 35);
        break;
      case 5:
      case 6:
        mOpis = "pretežno oblačno";
        mTemp = getRandomArbitrary(15, 25);
        break;
      case 8:
        mOpis = "sneženo";
        mTemp = getRandomArbitrary(-15, -25);
        break;
      default:
        mOpis = "oblačno";
        mTemp = getRandomArbitrary(5, 30);
    }
    
    var rezultat = {
      opis: mOpis,
      ikona: vremenske_slike[mOpis],
      temperatura: mTemp
    };
    odgovor.send(rezultat);
  }
});

function getRandomArbitrary(min, max) {
    return Math.random() * (max + 1 - min) + min;
}

function firstDigit(n) 
{ 
    // Remove last digit from number 
    // till only one digit is left 
    while (n >= 10)  
        n /= 10;
      
    // return the first digit 
    return n; 
} 


// Prikaz zgodovine
streznik.get("/zgodovina", function (zahteva, odgovor) {
  
  
  var con = "<table>";
  
  seznam_krajev.forEach(function(item){
    con += "<tr><th>" + item.postnaStevilka + "</th><th>" + item.kraj +"</th></tr>";
  });
  
  con += "</table>";
  
  odgovor.send(seznam_krajev);
});


// Prikaz strani z zemljevidom
streznik.get("/zemljevid-vremena", function (zahteva, odgovor) {
  odgovor.render('zemljevid-vremena');
});


streznik.get("/zabelezi-zadnji-kraj/:json", function(zahteva, odgovor) {
  var rezultat = zahteva.params.json;
  seznam_krajev.push(JSON.parse(rezultat));
  odgovor.send({steviloKrajev: seznam_krajev.length});
});


streznik.listen(process.env.PORT, function () {
  console.log("Strežnik je pognan!");
});
